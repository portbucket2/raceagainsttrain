using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

#if AP_GAMEANALYTICS_SDK_INSTALLED
using GameAnalyticsSDK;
#endif
#if AP_LIONSTUDIO_SDK_INSTALLED
using LionStudios.Suite.Analytics;
using LionStudios.Suite.Debugging;
#endif

public class GameManager : APManager
{
    public static Action<Trainbogie> OnNewBogieSelect;
    public static Action<Trainbogie> OnCurrentBogieComplete;
    public static Action OnStartFire, OnStopFire;
    public GameObject coinPrefab, shieldPrefab, twoXPrefab;
    public Transform coinUI, shieldUI, towxUI;


    public PlayerController playerController;
    public Trainbogie currentBogie;

    public void SelectNextBogie(Trainbogie trainbogie)
    {
        currentBogie = trainbogie;
        OnNewBogieSelect?.Invoke(trainbogie);
    }

    public override void Awake()
    {
        base.Awake();
        ChangeGameState(GameState.NONE);
    }

    public override void None()
    {
        base.None();
        ChangeGameState(GameState.GAME_DATA_LOADED);

        gameStartingUI.SetBool("Hide", false);
    }

    public override void GameDataLoad()
    {
        base.GameDataLoad();
        ChangeGameState(GameState.GAME_INITIALIZED);

        APTools.poolManager.PrePopulateItem(coinPrefab, 5);
        APTools.poolManager.PrePopulateItem(shieldPrefab, 5);
        APTools.poolManager.PrePopulateItem(twoXPrefab, 5);
    }

    public override void OnCompleteATask()
    {
        totalCompletedTask++;
        OnCompleteTask?.Invoke();

        if (totalCompletedTask.Equals(totalGivenTask))
        {
            OnStopFire?.Invoke();
            OnCurrentBogieComplete?.Invoke(currentBogie);
        }
    }

    public override void GameStart()
    {
        base.GameStart();

        gameStartingUI.SetBool("Hide", true);
        gamePlayUI.SetBool("Hide", false);

#if AP_GAMEANALYTICS_SDK_INSTALLED
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + (gameplayData.currentLevelNumber + 1));
#endif
#if AP_LIONSTUDIO_SDK_INSTALLED
        PlayerPrefs.SetInt((gameplayData.currentLevelNumber + 1).ToString(), PlayerPrefs.GetInt((gameplayData.currentLevelNumber + 1).ToString(), 0) + 1);
        LionAnalytics.LevelStart(gameplayData.currentLevelNumber + 1, PlayerPrefs.GetInt((gameplayData.currentLevelNumber + 1).ToString()));
#endif
    }

    public override void GameOver()
    {
        base.GameOver();

        if (gameplayData.isGameoverSuccess)
        {
            gameplayData.currentLevelNumber++;
            gameSuccessUI.SetBool("Hide", false);
            gameOverEffect.Play();
            PlayThisSoundEffect(gameWinAudioClip);


#if AP_GAMEANALYTICS_SDK_INSTALLED
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + gameplayData.currentLevelNumber);
#endif
#if AP_LIONSTUDIO_SDK_INSTALLED
            LionAnalytics.LevelComplete(gameplayData.currentLevelNumber, 1);
#endif
        }
        else
        {
            gameFaildUI.SetBool("Hide", false);
            PlayThisSoundEffect(gameLoseFailAudioClip);


#if AP_GAMEANALYTICS_SDK_INSTALLED
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + (gameplayData.currentLevelNumber + 1));
#endif
#if AP_LIONSTUDIO_SDK_INSTALLED
            LionAnalytics.LevelFail(gameplayData.currentLevelNumber + 1, PlayerPrefs.GetInt((gameplayData.currentLevelNumber + 1).ToString()));
#endif
        }
    }

    public override void ReloadLevel()
    {
        base.ReloadLevel();

#if AP_LIONSTUDIO_SDK_INSTALLED
        LionAnalytics.LevelRestart(gameplayData.currentLevelNumber + 1, PlayerPrefs.GetInt((gameplayData.currentLevelNumber + 1).ToString()));
#endif
    }

    public void CollectPickups(Vector3 position, RoadPickupType roadPickupType)
    {
        GameObject go;
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(position);
        switch (roadPickupType)
        {
            case RoadPickupType.ARMOR:
                go = APTools.poolManager.Instantiate(shieldPrefab);
                go.transform.parent = gamePlayUI.transform;
                go.transform.position = screenPosition;
                go.transform.DOMove(shieldUI.position, ConstantManager.DEFAULT_ANIMATION_TIME / 2).OnComplete(() => {

                    APTools.poolManager.Destroy(go);
                });
                break;
            case RoadPickupType.COIN:
                go = APTools.poolManager.Instantiate(coinPrefab);
                go.transform.parent = gamePlayUI.transform;
                go.transform.position = screenPosition;
                go.transform.DOMove(coinUI.position.ModifyThisVector(-100f, 0, 0), ConstantManager.DEFAULT_ANIMATION_TIME / 2).OnComplete(()=> {

                    APTools.poolManager.Destroy(go);
                });
                break;
            case RoadPickupType.DMG:
                go = APTools.poolManager.Instantiate(twoXPrefab);
                go.transform.parent = gamePlayUI.transform;
                go.transform.position = screenPosition;
                go.transform.DOMove(towxUI.position, ConstantManager.DEFAULT_ANIMATION_TIME / 2).OnComplete(() => {

                    APTools.poolManager.Destroy(go);
                });
                break;
            case RoadPickupType.LIFE:
                break;
        }
    }
}
