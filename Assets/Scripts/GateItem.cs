using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GateItem : APBehaviour
{
    public GateController gate;
    public SpriteRenderer spriteRenderer;
    public Sprite itemIcon;

    GateItemType gateItemType;
    GateData gateData;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void OnTriggerEnter(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        //gate.gameObject.SetActive(false);
        gameObject.SetActive(false);
        switch (gateData.gateItemType)
        {
            case GateItemType.PISTOL:
            case GateItemType.SHOTGUN:
            case GateItemType.SMG:
            case GateItemType.ASSAULT_RIFLE:
            case GateItemType.LAUNCHER:
                ((GameManager)gameManager).playerController.ActivateWeapon(gateData);
                break;
            case GateItemType.RAMP:
                ((GameManager)gameManager).playerController.JumpByRamp();
                break;
        }
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(GateData gateData)
    {
        //if (Random.Range(0, 2) == 1)
        //{
        //    gameObject.SetActive(false);
        //    return;
        //}

        gameObject.SetActive(true);
        this.gateData = gateData;
        gateItemType = gateData.gateItemType;
        spriteRenderer.sprite = gateData.gateSprite;
    }
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
