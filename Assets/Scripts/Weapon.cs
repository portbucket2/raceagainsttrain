using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : APBehaviour
{
    public GateItemType gateItemType;
    public Transform bulletSpawnPoint;
    public GameObject bulletPrefab, enemyBulletPrefab;
    public float bulletSpeed;
    public Animator anim;
    public float damageAmount = 20f;

    [HideInInspector]
    public int damageMultiplyer = 1;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        APTools.poolManager.PrePopulateItem(bulletPrefab, 4);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameOver()
    {
        base.OnGameOver();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize()
    {
        damageMultiplyer = 1;
    }

    public void DrawMuzzle()
    {
        // Show Muzzle
        anim.SetTrigger("FireOn");
    }
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
