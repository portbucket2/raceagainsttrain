using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingHand : APBehaviour
{
    public Transform aimTarget;
    public Vector3 aimOffset;
    public bool firing;

    Animator anim;
    Transform chest;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        if (anim == null)
        {
            anim = GetComponent<Animator>();
            chest = anim.GetBoneTransform(HumanBodyBones.Chest);
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        //anim = GetComponent<Animator>();
        //chest = anim.GetBoneTransform(HumanBodyBones.Chest);
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (!firing)
            return;
        if (!aimTarget)
            return;

        chest.LookAt(aimTarget);
        chest.rotation *= Quaternion.Euler(aimOffset);
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnStartFire()
    {
        base.OnStartFire();
        firing = true;
    }

    public override void OnStopFire()
    {
        base.OnStopFire();
        firing = false;
    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(bool boss, Weapon weapon)
    {
        if (boss)
        {
            switch (weapon.gateItemType)
            {
                case GateItemType.SHOTGUN:
                    aimOffset = new Vector3(0, 55, 0);
                    break;
                case GateItemType.PISTOL:
                case GateItemType.SMG:
                case GateItemType.ASSAULT_RIFLE:
                case GateItemType.LAUNCHER:
                case GateItemType.RAMP:
                    break;
            }
        }
        else
        {
            switch (weapon.gateItemType)
            {
                case GateItemType.PISTOL:
                    aimOffset = new Vector3(0, 0, 0);
                    break;
                case GateItemType.LAUNCHER:
                case GateItemType.SHOTGUN:
                case GateItemType.SMG:
                case GateItemType.ASSAULT_RIFLE:
                    break;
                case GateItemType.RAMP:
                    break;
            }
        }
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
