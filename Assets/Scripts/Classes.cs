using System;
using UnityEngine;

[Serializable]
public class TrainBogieSkin
{
    public Transform body;
    public Transform enemySpawnPoints;
}

[Serializable]
public class GateData
{
    public GateItemType gateItemType;
    public Sprite gateSprite;
}