using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    public Transform secondaryGunHolder;

    [HideInInspector]
    public Weapon secondaryCurrentWeapon;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        aimCrossHair = Instantiate(aimPrefab, transform);
        aim = aimCrossHair.GetComponent<EnemyAim>();
        aim.Initialize(this);

        int gunIndex = Random.Range(0, gunHolder.childCount);
        int deathIndex = Random.Range(0, 3);

        gunHolder.ActiveChildByIndex(gunIndex);
        currentWeapon = gunHolder.transform.GetChild(gunIndex).GetComponent<Weapon>();

        secondaryGunHolder.ActiveChildByIndex(gunIndex);
        secondaryCurrentWeapon = secondaryGunHolder.transform.GetChild(gunIndex).GetComponent<Weapon>();

        anim.SetFloat("GunID", gunIndex);
        anim.SetFloat("DeathID", deathIndex);
             
    }

    // Start is called before the first frame update
    public override void Start()
    {
        int skinIndex = gameplayData.currentLevelNumber % enemySkins.Length;
        //Debug.LogError(skinIndex);
        for (int i = 0; i < enemySkins.Length; i++)
        {
            enemySkins[i].SetActive(i == skinIndex);
        }

        aimingHand.Initialize(true, currentWeapon);
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();

    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public override bool DamageHealth(float damageAmount)
    {
        bool isDead = false;
        //Debug.LogError(this.name + ":" + damageAmount + "->" + (damageAmount/4f));
        health -= (damageAmount / 4f);
        health = Mathf.Clamp(health, 0, 100);

        if (health == 0)
        {
            isDead = true;
            Die();
        }
        return isDead;
    }

    public void SecondaryFireABullet()
    {
        secondaryCurrentWeapon.DrawMuzzle();
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
