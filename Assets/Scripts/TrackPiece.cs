using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TrackPiece : APBehaviour
{
    public Transform endOfTrackPiece, roadPickupsHolder;
    public GateController gateController;
    public RoadPickup[] roadPickups;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(bool withGate)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        roadPickupsHolder.transform.localPosition = new Vector3(
            Random.Range(0, 2) == 0? -1.5f : 1.5f, roadPickupsHolder.localPosition.y, roadPickupsHolder.localPosition.z);

        RoadPickupType roadPickupType;
        if (Random.Range(0,5) == 4)
            roadPickupType = (RoadPickupType)Random.Range(1, (int)RoadPickupType.End_of_Enum);
        else
            roadPickupType = RoadPickupType.COIN;

        for (int i = 0; i < roadPickups.Length; i++)
        {
            roadPickups[i].Initialize(roadPickupType);
        }
        if (withGate)
        {
            gateController.Initialize();
        }
        else
            gateController.gameObject.SetActive(false);

    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
