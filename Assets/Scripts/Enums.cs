using System;
using UnityEngine;

public enum RoadPickupType
{
    COIN,
    ARMOR,
    DMG,
    LIFE,
    End_of_Enum
}

public enum GateItemType
{
    PISTOL,
    SHOTGUN,
    SMG,
    ASSAULT_RIFLE,
    LAUNCHER,
    RAMP,
    END_OF_ENUM
}
