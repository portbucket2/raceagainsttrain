using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : APBehaviour
{
    public GateItem leftGate, rightGate;

    public GateData[] gateDatas;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
    
    
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize()
    {
        gameObject.SetActive(true);
        int leftGateIndex, rightGateIndex;
        leftGateIndex = Random.Range(0, (int)GateItemType.END_OF_ENUM);
        do
        {
            rightGateIndex = Random.Range(0, (int)GateItemType.END_OF_ENUM);

        } while (leftGateIndex == rightGateIndex);

        leftGate.Initialize(gateDatas[leftGateIndex]);
        rightGate.Initialize(gateDatas[rightGateIndex]);
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
