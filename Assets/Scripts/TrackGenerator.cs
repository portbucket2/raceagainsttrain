using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class TrackGenerator : APBehaviour
{
    public GameObject trackPiecePrefab, trainbogiePrefab, trainEnginPrefab;
    public PlayerController playerController;
    public Vector3 lastBogiePosition = new Vector3(-5f, 0, 20);
    public bool detachedBogieOnComplete = false;

    Transform engin;
    List<TrackPiece> trackPieces = new List<TrackPiece>();
    List<Trainbogie> bogies;
    Vector3 lastTrackPiecePosition = Vector3.zero;
    float trackCheckingDistance = 100, gateCooldownTime = -1;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        if (gateCooldownTime > 0)
            gateCooldownTime -= Time.deltaTime;

        if (trackPieces.Count > 0)
        {
            if (trackPieces[0].transform.position.z < playerController.transform.position.z - trackCheckingDistance)
            {
                trackPieces[0].transform.position = trackPieces[trackPieces.Count - 1].endOfTrackPiece.position;
                TrackPiece lastTrackPiece = trackPieces[0];

                lastTrackPiece.Initialize(gateCooldownTime <= 0);
                if (gateCooldownTime <= 0)
                {
                    gateCooldownTime = 5;
                }

                trackPieces.RemoveAt(0);
                trackPieces.Add(lastTrackPiece);
            }
        }
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();

        GenerateTrack();
        GenerateBogies();
    }

    public override void OnCurrentBogieComplete(Trainbogie trainbogie)
    {
        base.OnCurrentBogieComplete(trainbogie);

        if (trainbogie != null && detachedBogieOnComplete)
        {
            //Detuch Bogie
            trainbogie.transform.parent = null;
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    void GenerateTrack()
    {
        trackPieces = new List<TrackPiece>();
        for (int i = 0; i < 6; i++)
        {
            GameObject go = Instantiate(trackPiecePrefab, transform);
            go.transform.position = lastTrackPiecePosition;

            TrackPiece trackPiece = go.GetComponent<TrackPiece>();
            lastTrackPiecePosition = trackPiece.endOfTrackPiece.position;

            trackPieces.Add(trackPiece);            
        }
    }

    void GenerateBogies()
    {
        bogies = new List<Trainbogie>();
        int bogieCount;

        if (gameplayData.currentLevelNumber % 2 == 0)
        {
            //Genarate Bogie for Calliy Level
            bogieCount = Random.Range(5, 7);
            //for (int i = 0; i < bogieCount; i++)
            //{
            //    GameObject _go = Instantiate(trainbogiePrefab, transform);
            //    _go.transform.position = lastBogiePosition;

            //    Trainbogie _trainbogie = _go.GetComponent<Trainbogie>();
            //    lastBogiePosition = _trainbogie.endOfBogie.position;
            //    bogies.Add(_trainbogie);
            //}
        }
        else
        {
            //Genarate Bogie for Boss Level
            bogieCount = 1;
            //for (int i = 0; i < bogieCount; i++)
            //{
            //    GameObject _go = Instantiate(trainbogiePrefab, transform);
            //    _go.transform.position = lastBogiePosition;

            //    Trainbogie _trainbogie = _go.GetComponent<Trainbogie>();
            //    lastBogiePosition = _trainbogie.endOfBogie.position;
            //    bogies.Add(_trainbogie);
            //}
        }
        for (int i = 0; i < bogieCount; i++)
        {
            GameObject _go = Instantiate(trainbogiePrefab, transform);
            _go.transform.position = lastBogiePosition;

            Trainbogie _trainbogie = _go.GetComponent<Trainbogie>();
            lastBogiePosition = _trainbogie.endOfBogie.position;
            bogies.Add(_trainbogie);
            _trainbogie.InitializedEmemy();
        }

        GameObject enginBody = Instantiate(trainEnginPrefab, transform);
        engin = enginBody.transform;

        enginBody.transform.position = lastBogiePosition;
        TrainEngine trainEngine = enginBody.GetComponent<TrainEngine>();
        lastBogiePosition = trainEngine.endofBogie.position;

        for (int i = 0; i < bogies.Count; i++)
        {
            bogies[i].transform.parent = engin;
        }
        playerController.bogies = bogies;

        TargetFollower targetFollower = Camera.main.GetComponent<TargetFollower>();
        targetFollower.followOffset = new Vector3(4, targetFollower.followOffset.y, targetFollower.followOffset.z);

        targetFollower.followTarget = bogies[0].transform;
        targetFollower.transform.eulerAngles = new Vector3(
            targetFollower.transform.eulerAngles.x, 0, targetFollower.transform.eulerAngles.z);

        ((GameManager)gameManager).playerController.transform.parent = bogies[0].transform;
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
