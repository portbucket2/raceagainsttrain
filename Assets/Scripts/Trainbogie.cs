using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Trainbogie : APBehaviour
{
    public GameObject challyEnemyPrefab, bossEnemyPrefab;
    public Transform endOfBogie;
    public TrainBogieSkin[] trainBogieSkins;

    //[HideInInspector]
    public List<Enemy> allEnemies;
    public Enemy targetEnemey;

    Trainbogie currentBogie;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnNewBogieSelect(Trainbogie trainbogie)
    {
        base.OnNewBogieSelect(trainbogie);

        currentBogie = trainbogie;
        if (currentBogie.Equals(this))
        {
            SelectNextEnemy();
        }
    }

    public override void OnStartFire()
    {
        base.OnStartFire();

        if (currentBogie.Equals(this))
        {
            for (int i = 0; i < allEnemies.Count; i++)
            {
                allEnemies[i].GetComponent<Animator>().SetTrigger("OnFire");
            }
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void SelectNextEnemy()
    {
        if(allEnemies.Count > 0)
        {
            targetEnemey = allEnemies[0];
        }
    }

    public void DamageTargetEnemy(float damageAmount)
    {
        if(targetEnemey == null)
        {
            targetEnemey = allEnemies[0];
        }

        //int enemyIndex = Random.Range(0, allEnemies.Count);
        if (targetEnemey.DamageHealth(damageAmount))
        {
            allEnemies.RemoveAt(0);
            SelectNextEnemy();
        }
    }

    public void InitializedEmemy()
    {
        int bogieSkinIndex = Random.Range(0, trainBogieSkins.Length);
        TrainBogieSkin trainBogieSkin = trainBogieSkins[bogieSkinIndex];
        for (int i = 0; i < trainBogieSkins.Length; i++)
        {
            trainBogieSkins[i].body.gameObject.SetActive(i == bogieSkinIndex);
        }

        allEnemies = new List<Enemy>();
        int enemyCount;

        if (gameplayData.currentLevelNumber % 2 == 0)
        {
            //Genarate Calliy for the Level
            enemyCount = Random.Range(1, trainBogieSkin.enemySpawnPoints.childCount + 1);
            for (int i = 0; i < trainBogieSkin.enemySpawnPoints.childCount; i++)
            {
                if (enemyCount < trainBogieSkin.enemySpawnPoints.childCount - i)
                {
                    bool drawEnemy = Random.Range(1, 3) == 1 ? true : false;
                    if (!drawEnemy)
                    {
                        continue;
                    }
                }
                --enemyCount;
                GameObject go = Instantiate(challyEnemyPrefab, trainBogieSkin.enemySpawnPoints.GetChild(i));
                go.transform.position = trainBogieSkin.enemySpawnPoints.GetChild(i).position;

                Enemy enemy = go.GetComponent<Enemy>();
                enemy.Initialized(this);

                allEnemies.Add(enemy);

            }
        }
        else
        {
            //Genarate Boss for the Level
            enemyCount = 1;
            for (int i = 0; i < trainBogieSkin.enemySpawnPoints.childCount; i++)
            {
                if (enemyCount < trainBogieSkin.enemySpawnPoints.childCount - i)
                {
                    bool drawEnemy = Random.Range(1, 3) == 1 ? true : false;
                    if (!drawEnemy)
                    {
                        continue;
                    }
                }
                --enemyCount;
                GameObject go = Instantiate(bossEnemyPrefab, trainBogieSkin.enemySpawnPoints.GetChild(i));
                go.transform.position = trainBogieSkin.enemySpawnPoints.GetChild(i).position;

                Enemy enemy = go.GetComponent<Enemy>();
                enemy.Initialized(this);

                allEnemies.Add(enemy);
                if (enemyCount == 0)
                    break;
            }
        }
    }
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
