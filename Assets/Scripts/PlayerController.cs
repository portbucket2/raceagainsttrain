using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerController : APBehaviour
{
    public Material shooterHitMat;
    public GameObject bulletTrailPrefab;
    public Animator stuntAnim, bikerAnim, shooterAnim;
    public Transform groundCheckPoint, stuntPoint, gunHolder, shieldVFX;
    public Weapon currentWeapon;
    public Sprite defaultGunSprite;
    public Image healthUI, twoXUI, shieldUI, gunUI;
    public TextMeshProUGUI startingCountText, coinText;
    public bool attachTrailToTrain;
    public AimingHand aimingHand;
    public float springJumpForce = 10;
    public GateData initialWeapon;

    Rigidbody rig;
    bool isMoving, isTryingToReachNextBogie, fireOnEnemy;
    float engineSpeed = 15;
    float engineForce = 1;
    float playerSideLimit = 1, dragSensitivity = 1;
    float xLimit = 2.2f;
    float health = 100;
    int totalCoinCollected = 0;

    [HideInInspector]
    public List<Trainbogie> bogies;
    Trainbogie currentBogie;
    TargetFollower targetFollower;
    int currentBogieIndex = -1;
    float bogieReachingOffsetZ = 5;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        Registar_For_Input_Callback();
        rig = GetComponent<Rigidbody>();
        targetFollower = Camera.main.GetComponent<TargetFollower>();

        stuntPoint.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -40);
        shieldVFX.gameObject.SetActive(false);

        APTools.poolManager.ResetPoolManager();
        APTools.poolManager.PrePopulateItem(bulletTrailPrefab, 10);

        ActivateWeapon(initialWeapon);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        shooterHitMat.color = new Color(1, 1, 1, 0);
        engineSpeed = ConstantManager.enginSpeed;
    }

    void Update()
    {
        if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        {
            stuntPoint.localPosition = Vector3.zero;
            transform.parent = null;

            targetFollower.followZ = AcceptedValueType.ONLY_POSITIVE;
            targetFollower.followOffset = new Vector3(3f, targetFollower.followOffset.y, targetFollower.followOffset.z);
            transform.position = new Vector3(transform.position.x, transform.position.y, targetFollower.transform.position.z - bogieReachingOffsetZ * 2);

            targetFollower.followTarget = transform;
            targetFollower.transform.eulerAngles = new Vector3(
                targetFollower.transform.eulerAngles.x, -15, targetFollower.transform.eulerAngles.z);

            isMoving = true;
            OnCurrentBogieComplete(new Trainbogie());

            gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        }

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;        

        if (isTryingToReachNextBogie)
        {
            if (transform.DistanceFrom(currentBogie.transform, APAxis.Z) < bogieReachingOffsetZ)
            {
                isTryingToReachNextBogie = false;
                stuntAnim.SetBool("WheelUpStant", false);
                engineForce = 1;
                GameManager.OnStartFire?.Invoke();
            }
        }

        if (fireOnEnemy)
        {
            //currentBogie.DamageAnRandomEnemy(currentWeapon.damageAmount * Time.deltaTime);            
        }
        shooterAnim.SetBool("FireOn", fireOnEnemy);

        healthUI.fillAmount = Mathf.InverseLerp(0, 100, health);
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (isMoving)
        {
            if (isTryingToReachNextBogie)
            {
                engineForce += Time.deltaTime;
            }
            float speed = transform.position.z + engineSpeed * Time.fixedDeltaTime * engineForce;

            Vector3 newPosition = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, speed), new Vector3(playerSideLimit, transform.position.y, speed), 0.25f);
            transform.position = newPosition;
        }
    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (other.gameObject.layer.Equals(ConstantManager.ENEMY_LAYER))
        {

        }
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnSwipeLeft()
    {
        base.OnSwipeLeft();

        //playerSideLimit = -xLimit;
    }

    public override void OnSwipeRight()
    {
        base.OnSwipeRight();

        //playerSideLimit = xLimit;
    }

    public override void OnDrag(Vector3 dragAmount)
    {
        base.OnDrag(dragAmount);

        playerSideLimit += dragAmount.x * Time.deltaTime;
        playerSideLimit = Mathf.Clamp(playerSideLimit, -xLimit, xLimit);
    }

    public override void OnGameStart()
    {
        base.OnGameStart();
    }

    public override void OnGameOver()
    {
        base.OnGameOver();

        if (gameplayData.isGameoverSuccess)
        {
            bikerAnim.SetTrigger("OnLevelComplete");
            shooterAnim.SetTrigger("OnLevelComplete");
        }
        else
        {
            targetFollower.followTarget = shooterAnim.transform;
            targetFollower.lookTarget = shooterAnim.transform;
            targetFollower.lookAtTarget = true;

            bikerAnim.transform.parent = null;
            shooterAnim.transform.parent = null;

            stuntAnim.transform.DOMove(new Vector3(stuntAnim.transform.position.x, stuntAnim.transform.position.y, stuntAnim.transform.position.z + 50), ConstantManager.DEFAULT_ANIMATION_TIME * 2);
            bikerAnim.transform.DOMove(new Vector3(bikerAnim.transform.position.x, 0, bikerAnim.transform.position.z), ConstantManager.DEFAULT_ANIMATION_TIME);
            shooterAnim.transform.DOMove(new Vector3(shooterAnim.transform.position.x, 0, shooterAnim.transform.position.z), ConstantManager.DEFAULT_ANIMATION_TIME);

            bikerAnim.SetTrigger("OnLevelIncomplete");
            shooterAnim.SetTrigger("OnLevelIncomplete");
        }
    }

    public override void OnStartFire()
    {
        base.OnStartFire();
        fireOnEnemy = true;
    }

    public override void OnStopFire()
    {
        base.OnStopFire();
        fireOnEnemy = false;
    }

    public override void OnNewBogieSelect(Trainbogie trainbogie)
    {
        base.OnNewBogieSelect(trainbogie);

        currentBogie = trainbogie;
        stuntAnim.SetBool("WheelUpStant", true);

        gameManager.totalCompletedTask = 0;
        gameManager.totalGivenTask = currentBogie.allEnemies.Count;
    }

    public override void OnCurrentBogieComplete(Trainbogie trainbogie)
    {
        base.OnCurrentBogieComplete(trainbogie);        

        currentBogieIndex++;
        if(currentBogieIndex < bogies.Count)
            ((GameManager)gameManager).SelectNextBogie(bogies[currentBogieIndex]);
        else
        {
            gameplayData.isGameoverSuccess = true;
            gameManager.ChangeGameState(GameState.GAME_PLAY_ENDED);
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    /// <summary>
    /// This function will be called from animation timeline.
    /// </summary>
    public void FireABullet()
    {
        currentWeapon.DrawMuzzle();

        aimingHand.aimTarget = currentBogie.targetEnemey.transform;
        currentBogie.DamageTargetEnemy(currentWeapon.damageAmount * (twoXUI.fillAmount > 0? 2 : 1));

        //GameObject bulletTrail = APTools.poolManager.Instantiate(bulletTrailPrefab);
        //bulletTrail.transform.parent = currentBogie.transform;

        //bulletTrail.transform.position = currentWeapon.bulletSpawnPoint.position;
        //bulletTrail.transform.LookAt(currentBogie.targetEnemey.transform);

        //ParticleSystem ps = bulletTrail.GetComponent<ParticleSystem>();
        //var main = ps.main;
        //main.simulationSpace = ParticleSystemSimulationSpace.Custom;
        //main.customSimulationSpace = currentBogie.transform;
        //ps.Play();

        GameObject bullet = Instantiate(currentWeapon.bulletPrefab, currentBogie.targetEnemey.transform);
        bullet.transform.position = currentWeapon.bulletSpawnPoint.position;
        bullet.transform.LookAt(currentBogie.targetEnemey.head);
        bullet.transform.DOLocalMove(Vector3.zero.ModifyThisVector(0, 1f, 0), currentWeapon.bulletSpeed).OnComplete(()=> {

            APTools.poolManager.Destroy(bullet);
        });
        //APTools.functionManager.ExecuteAfterSecond(0.01f, () =>
        //{
        //    bulletTrail.transform.position = currentBogie.targetEnemey.head.position;
        //    APTools.functionManager.ExecuteAfterSecond(1f, () =>
        //    {
        //        ps.Stop();
        //        APTools.poolManager.Destroy(bulletTrail);
        //    });
        //});
    }

    /// <summary>
    /// This functing will be called only from animation timeline
    /// </summary>
    public void ForceBikeToNextBogie()
    {
        isTryingToReachNextBogie = true;
    }

    public void CollectThisRoadPickup(RoadPickup roadPickup)
    {
        roadPickup.gameObject.SetActive(false);
        ((GameManager)gameManager).CollectPickups(roadPickup.transform.position, roadPickup.roadPickupType);
        switch (roadPickup.roadPickupType)
        {
            case RoadPickupType.ARMOR:
                CollectShield(roadPickup);
                break;
            case RoadPickupType.COIN:
                CollectCoin(roadPickup);
                break;
            case RoadPickupType.DMG:
                SetGunDMG(roadPickup);
                break;
            case RoadPickupType.LIFE:
                health = 100;
                break;
        }
    }

    public void JumpByRamp()
    {
        if (!isTryingToReachNextBogie && isMoving)
        {
            RaycastHit? hit = groundCheckPoint.GetGroundHitPoint();
            if (hit != null)
            {
                if (((RaycastHit)hit).distance < 1.25f)
                {
                    Vector3 velocity = rig.velocity;
                    velocity.y = springJumpForce;
                    rig.velocity = velocity;
                }
            }
        }
    }

    public void ActivateWeapon(GateData gateData)
    {

        for (int i = 0; i < gunHolder.childCount; i++)
        {
            Weapon weapon = gunHolder.GetChild(i).GetComponent<Weapon>();
            if (weapon.gateItemType.Equals(gateData.gateItemType))
            {
                currentWeapon = weapon;
                currentWeapon.Initialize();
                gunUI.sprite = gateData.gateSprite;
            }

            gunHolder.GetChild(i).gameObject.SetActive(weapon.gateItemType.Equals(gateData.gateItemType));
        }
        shooterAnim.SetFloat("GunID", (int)gateData.gateItemType);
    }

    void CollectCoin(RoadPickup roadPickup, int amount = 1)
    {
        totalCoinCollected += amount;
        coinText.text = totalCoinCollected.ToString();
    }

    void SetGunDMG(RoadPickup roadPickup, int multiplayer = 1)
    {
        twoXUI.fillAmount = 1;
        DOTween.To(() => twoXUI.fillAmount, x => twoXUI.fillAmount = x, 0, 5);
    }

    void CollectShield(RoadPickup roadPickup)
    {
        if (!shieldVFX.gameObject.activeSelf)
        {
            shieldUI.fillAmount = 1;
            shieldVFX.gameObject.SetActive(true);
            DOTween.To(() => shieldUI.fillAmount, x => shieldUI.fillAmount = x, 0, 5).SetEase(Ease.Linear).OnComplete(() =>
            {
                shieldVFX.gameObject.SetActive(false);
            });
        }
    }

    public void DamageHealth(float damageAmount)
    {
        if (shieldVFX.gameObject.activeSelf)
            return;

        bool isDead = false;
        //Debug.LogError(this.name +":" + damageAmount + "->" + (damageAmount / 4f));
        health -= damageAmount / 2f;
        health = Mathf.Clamp(health, 0, 100);
        shooterHitMat.color = new Color(1, 1, 1, 1);
        APTools.functionManager.ExecuteAfterSecond(0.1f, ()=> {

            shooterHitMat.color = new Color(1,1,1,0);
        });

        if (health == 0)
        {
            isDead = true;
            gameplayData.isGameoverSuccess = false;
            gameManager.ChangeGameState(GameState.GAME_PLAY_ENDED);
        }
    }
    
    #endregion ALL SELF DECLEAR FUNCTIONS

}
