using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : APBehaviour
{
    public GameObject[] enemySkins;
    public Transform head;
    public Animator anim;
    public Image healthUI;
    public GameObject aimPrefab, gunLinePrefab;
    public Transform gunHolder;
    public AimingHand aimingHand;

    [HideInInspector]
    public Weapon currentWeapon;
    [HideInInspector]
    public GameObject aimCrossHair;
    [HideInInspector]
    public EnemyAim aim;

    GameObject aimTargetPoint, aimMovingPoint;
    Trainbogie currentBogie, myBogie;
    Vector3 newAimPosition;

    public float health = 100, aimXLimit = 2.5f, aimZLimit = 5, aimCrossHairMovmentSpeed = 0.05f;
    float aimSideLimit = 0.5f;
    bool firing;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        aimCrossHair = Instantiate(aimPrefab, transform);
        aim = aimCrossHair.GetComponent<EnemyAim>();
        aim.Initialize(this);

        int gunIndex = Random.Range(0, gunHolder.childCount);
        int deathIndex = Random.Range(0, 3);

        gunHolder.ActiveChildByIndex(gunIndex);
        currentWeapon = gunHolder.transform.GetChild(gunIndex).GetComponent<Weapon>();

        anim.SetFloat("GunID", gunIndex);
        anim.SetFloat("DeathID", deathIndex);

        int skinIndex = Random.Range(0, enemySkins.Length);
        for (int i = 0; i < enemySkins.Length; i++)
        {
            enemySkins[i].SetActive(i == skinIndex);
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        aimingHand.Initialize(false, currentWeapon);
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        healthUI.fillAmount = Mathf.InverseLerp(0, 100, health);

        if (firing)
        {
            if (aimCrossHair.transform.position.DistanceFrom(aimMovingPoint.transform.position) < 0.1f)
            {
                newAimPosition = aimCrossHair.transform.position;
                newAimPosition.y = 0;

                newAimPosition.x = aimTargetPoint.transform.position.x + (Random.Range(0,2) == 0? Random.Range(-aimXLimit, -aimXLimit + aimSideLimit) : Random.Range(aimXLimit - aimSideLimit, aimXLimit));
                newAimPosition.z = aimTargetPoint.transform.position.z + Random.Range(-aimZLimit, aimZLimit);

                aimMovingPoint.transform.position = newAimPosition;
            }
            aimCrossHair.transform.position = Vector3.Lerp(
                aimCrossHair.transform.position,
                aimMovingPoint.transform.position,
                aimCrossHairMovmentSpeed);
        }
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnNewBogieSelect(Trainbogie trainbogie)
    {
        base.OnNewBogieSelect(trainbogie);

        currentBogie = trainbogie;
    }

    public override void OnStartFire()
    {
        base.OnStartFire();

        if (currentBogie.Equals(myBogie))
        {
            aimTargetPoint = new GameObject("AimTargetPoint");
            aimTargetPoint.transform.parent = transform;
            aimMovingPoint = new GameObject("AimMovingPoint");
            aimMovingPoint.transform.parent = transform;

            newAimPosition = ((GameManager)gameManager).playerController.transform.position;
            newAimPosition.y = 0;
            newAimPosition.x = 0;
            aimTargetPoint.transform.position = newAimPosition;
            aimMovingPoint.transform.position = newAimPosition;

            newAimPosition.x = aimTargetPoint.transform.position.x + (Random.Range(0, 2) == 0 ? Random.Range(-aimXLimit, -aimXLimit + aimSideLimit) : Random.Range(aimXLimit - aimSideLimit, aimXLimit)); ;
            newAimPosition.z = aimTargetPoint.transform.position.z + Random.Range(-aimZLimit, -aimZLimit);

            aimMovingPoint.transform.position = newAimPosition;
            aimCrossHair.transform.position = newAimPosition;
            aimCrossHair.SetActive(true);

            firing = true;
            anim.SetBool("FireOn", true);
        }

    }

    public override void OnStopFire()
    {
        base.OnStopFire();

        firing = false;
        anim.SetBool("FireOn", false);

        aimCrossHair.SetActive(false);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialized(Trainbogie trainbogie)
    {
        myBogie = trainbogie;
    }

    /// <summary>
    /// This function will be called from animation timeline.
    /// </summary>
    public void FireABullet()
    {
        currentWeapon.DrawMuzzle();
        //aimingHand.aimTarget = ((GameManager)gameManager).playerController.transform;
        aimingHand.aimTarget = aim.transform;

        Transform player = ((GameManager)gameManager).playerController.transform;
        GameObject bullet = Instantiate(currentWeapon.enemyBulletPrefab, player);
        bullet.transform.position = currentWeapon.bulletSpawnPoint.position;
        bullet.transform.LookAt(((GameManager)gameManager).playerController.transform);
        bullet.transform.parent = aim.transform;
        bullet.transform.DOLocalMove(Vector3.zero.ModifyThisVector(0, 1f, 0), currentWeapon.bulletSpeed).OnComplete(() => {

            APTools.poolManager.Destroy(bullet);
            aim.ShowHitParticle();
        });

        RaycastHit hit;
        if(Physics.Raycast(
            new Ray(new Vector3(aimCrossHair.transform.position.x, -10, aimCrossHair.transform.position.z), Vector3.up),
            out hit, Mathf.Infinity, 1 << ConstantManager.PLAYER_LAYER))
        {
            ((GameManager)gameManager).playerController.DamageHealth(currentWeapon.damageAmount);
            //Debug.LogError("Player damage: " + currentWeapon.damageAmount);
        }
    }

    public virtual bool DamageHealth(float damageAmount)
    {
        bool isDead = false;
        //Debug.LogError(this.name + ":" + damageAmount + "->" + (damageAmount));
        health -= damageAmount;
        health = Mathf.Clamp(health, 0, 100);

        if (health == 0)
        {
            isDead = true;
            Die();
        }
        return isDead;
    }

    public void Die()
    {
        OnStopFire();
        gameManager.OnCompleteATask();
        healthUI.transform.parent.parent.parent.gameObject.SetActive(false);

        anim.SetTrigger("OnDeath");
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
