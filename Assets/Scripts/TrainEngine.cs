using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TrainEngine : APBehaviour
{
    public Transform endofBogie;

    float engineSpeed = 10, engineForce = 1;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        engineSpeed = ConstantManager.enginSpeed;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        //if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
        //    return;

        float speed = transform.position.z + engineSpeed * Time.fixedDeltaTime * engineForce;

        transform.position = new Vector3(transform.position.x, transform.position.y, speed);
    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameStart()
    {
        base.OnGameStart();

        //DOTween.To(() => engineForce, x => engineForce = x, 1, 3).SetEase(Ease.InExpo);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
