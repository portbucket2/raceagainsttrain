using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPickup : APBehaviour
{
    public RoadPickupType roadPickupType;
    //public Sprite twoXActiveSprite, twoXDecativeSprite, shielActiveSprite, shieldDeactiveSprite;

    int pickupIndex;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        ((GameManager)gameManager).playerController.CollectThisRoadPickup(this);       

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(RoadPickupType roadPickupType)
    {
        //if(Random.Range(0,2) == 1)
        //{
        //    gameObject.SetActive(false);
        //    return;
        //}
        gameObject.SetActive(true);

        //if (roadPickupType == null)
        //    pickupIndex = Random.Range(0, (int)RoadPickupType.End_of_Enum);
        //else
        pickupIndex = (int)roadPickupType;

        for (int i = 0; i < (int)RoadPickupType.End_of_Enum; i++)
        {
            transform.GetChild(i).gameObject.SetActive(i == pickupIndex);
        }
        this.roadPickupType = roadPickupType;
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
